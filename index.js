import express from 'express'
import cors from 'cors'
import bodyParser from 'body-parser'
import cookieParser from 'cookie-parser'
import routes from './routes/index.js'
import dotenv from 'dotenv'
import db from './models/index.js'
import fs from 'fs'
dotenv.config()

const app = express()
const APP_HOST = process.env.APP_HOST ?? 'http://127.0.0.1'
const APP_PORT = process.env.APP_PORT ?? 5000
const imgDir = './images/'

db.sequelize.sync().then(() => console.log('[CONNECT] Database Synchronized')).catch((err) => console.log(err))

app.use(cors({ credentials: true, origin: `${APP_HOST}:${APP_PORT}` }))
app.use(cookieParser())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(express.json())
app.use((req, res, next) => {
	res.header("Access-Control-Allow-Origin", "*")
	res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT")
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, contentType,Content-Type, Accept, Authorization")
	next()
})
app.use(routes)

const checkDir = async () => {
	console.log(`[CHECKING] Checking Directory for Storage`)
	if (!fs.existsSync(imgDir)) {
		await fs.mkdirSync(imgDir)
		await console.log(`[SUCCESS] Directory Created`)
	} else {
		await console.log(`[WARNING] Directory Already Exists`)
	}
}

app.listen(APP_PORT, async () => {
	await checkDir()
	await console.log(`[RUNNING] ${APP_HOST}:${APP_PORT}`)
})