import express from "express"
import { all, allBy, store, destroy, allFor, countPosts } from "../controllers/PostController.js"
import multerHelper from "../middlewares/MulterReq.js"

const router = express.Router()
router.get('/', all)
router.get('/by/:userId', allBy)
router.get('/for/:userId', allFor)
router.get('/count/:userId', countPosts)
router.post('/', multerHelper, store)
router.delete('/:id', destroy)

export default router