import express from "express"
import { all, store, show, update, destroy } from "../controllers/RoleController.js"

const router = express.Router()
router.get('/', all)
router.post('/', store)
router.get('/:id', show)
router.put('/:id', update)
router.delete('/:id', destroy)

export default router