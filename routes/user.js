import express from "express"
import { all, store, show, update, destroy, showByUsername, search } from "../controllers/UserController.js"
import { verifyToken } from "../middlewares/VerifyToken.js"

const router = express.Router()
router.get('/', verifyToken, all)
router.post('/', verifyToken, store)
router.get('/:id', verifyToken, show)
router.post('/search', verifyToken, search)
router.get('/show/:username', verifyToken, showByUsername)
router.put('/:id', verifyToken, update)
router.delete('/:id', verifyToken, destroy)

export default router