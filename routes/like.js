import express from "express"
import { checkLikePost, dislikePost, likePost, countLike } from "../controllers/LikeController.js"

const router = express.Router()

router.post('/check/:userId/:postId', checkLikePost)
router.post('/like/:userId/:postId', likePost)
router.post('/dislike/:userId/:postId', dislikePost)
router.post('/count/:postId', countLike)

export default router