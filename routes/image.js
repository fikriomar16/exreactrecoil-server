import express from "express"
import path, { basename as _basename, join } from 'path'
import { fileURLToPath } from 'url'

const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)

const router = express.Router()
router.get('/:url', async (req, res) => {
	const path = join(__dirname, `../images/${req.params.url}`)
	res.sendFile(path)
})

export default router