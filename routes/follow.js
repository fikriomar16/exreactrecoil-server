import express from "express"
import { checkFollowingUser, doFollowUser, doUnfollowUser, countFollower, countFollowing } from "../controllers/FollowController.js"

const router = express.Router()

router.post('/check/:userId/:following_userId', checkFollowingUser)
router.post('/follow/:userId/:following_userId', doFollowUser)
router.post('/unfollow/:userId/:following_userId', doUnfollowUser)
router.get('/count/following/:userId', countFollowing)
router.get('/count/follower/:userId', countFollower)

export default router