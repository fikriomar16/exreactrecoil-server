import { Router } from 'express'
import roleRoute from './role.js'
import userRoute from './user.js'
import authRoute from './auth.js'
import tokenRoute from './token.js'
import postRoute from './post.js'
import imageRoute from './image.js'
import likeRoute from './like.js'
import followRoute from './follow.js'

const router = Router()

router.get('/', async (req, res) => {
	res.json({
		status: 'success',
		app: process.env.APP_NAME ?? process.env.npm_package_description,
		message: 'API Connected......'
	})
})
router.use('/roles', roleRoute)
router.use('/users', userRoute)
router.use('/posts', postRoute)
router.use('/likes', likeRoute)
router.use('/follows', followRoute)
router.use('/auth', authRoute)
router.use('/token', tokenRoute)
router.use('/image', imageRoute)

export default router