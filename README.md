# Server-Side App

## _Usage_
* run `yarn install` or `npm install` to install dependencies
* run `yarn dev` or `npm dev` to run (nodemon for dev mode)
* run `yarn start` or `npm start` to run (for prod mode)
* backend service will running at port `5000` as default