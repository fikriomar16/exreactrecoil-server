import db from './index.js'

const { sequelize, Sequelize } = db
const Like = sequelize.define('likes', {
	postId: {
		type: Sequelize.INTEGER,
		primaryKey: true
	},
	userId: {
		type: Sequelize.INTEGER,
		primaryKey: true
	},
})
export default Like