import db from './index.js'

const { sequelize, Sequelize } = db
const Follow = sequelize.define('follows', {
	userId: {
		type: Sequelize.INTEGER,
		primaryKey: true
	},
	following_userId: {
		type: Sequelize.INTEGER,
		primaryKey: true
	},
})
export default Follow