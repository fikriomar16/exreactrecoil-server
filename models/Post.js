import db from './index.js'

const { sequelize, Sequelize } = db
const Post = sequelize.define('posts', {
	id: {
		type: Sequelize.INTEGER,
		autoIncrement: true,
		primaryKey: true
	},
	title: {
		type: Sequelize.STRING
	},
	slug: {
		type: Sequelize.STRING,
		unique: true
	},
	caption: {
		type: Sequelize.TEXT
	},
	photo: {
		type: Sequelize.STRING
	},
	userId: {
		type: Sequelize.INTEGER
	}
})
export default Post