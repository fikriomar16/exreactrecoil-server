import db from './index.js'

const { sequelize, Sequelize } = db
const User = sequelize.define('users', {
	id: {
		type: Sequelize.BIGINT,
		autoIncrement: true,
		primaryKey: true
	},
	roleId: {
		type: Sequelize.INTEGER
	},
	name: {
		type: Sequelize.STRING
	},
	username: {
		type: Sequelize.STRING,
		unique: true
	},
	email: {
		type: Sequelize.STRING,
		unique: true
	},
	password: {
		type: Sequelize.STRING
	},
	remember_token: {
		type: Sequelize.TEXT
	}
})
export default User