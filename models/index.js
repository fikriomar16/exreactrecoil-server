'use strict';

import { readdirSync } from 'fs';
import { basename as _basename, join } from 'path';
import Sequelize, { DataTypes } from 'sequelize';
import { env as _env } from 'process';
import path from 'path'
import { fileURLToPath } from 'url'

const configjson = await import('../config/config.json', {
	assert: {
		type: 'json'
	}
})
const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)
const basename = _basename(__filename);
const env = _env.NODE_ENV || 'development';
const convert = JSON.stringify(configjson.default)
const config = JSON.parse(convert)[env]
const db = {};

let sequelize;
if (config.use_env_variable) {
	sequelize = new Sequelize(_env[config.use_env_variable], config);
} else {
	sequelize = new Sequelize(config.database, config.username, config.password, config);
}
readdirSync(__dirname)
	.filter(file => {
		return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
	})
	.forEach(async file => {
		// const model = await require(join(__dirname, file))(sequelize, DataTypes);
		// db[model.name] = model;
		const temp = await import(join(__dirname, file))
		const model = new temp.default(sequelize, DataTypes)
		db[model.name] = model
	});

Object.keys(db).forEach(modelName => {
	if (db[modelName].associate) {
		db[modelName].associate(db);
	}
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

export default db;
