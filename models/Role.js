import db from './index.js'

const { sequelize, Sequelize } = db
const Role = sequelize.define('roles', {
	id: {
		type: Sequelize.INTEGER,
		autoIncrement: true,
		primaryKey: true
	},
	name: {
		type: Sequelize.STRING
	}
})
export default Role