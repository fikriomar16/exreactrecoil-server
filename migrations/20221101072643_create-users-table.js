'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
	async up(queryInterface, Sequelize) {
		await queryInterface.createTable('users', {
			id: {
				type: Sequelize.DataTypes.BIGINT,
				primaryKey: true,
				autoIncrement: true
			},
			roleId: {
				type: Sequelize.DataTypes.INTEGER
			},
			name: {
				type: Sequelize.DataTypes.STRING(60),
				allowNull: true,
			},
			username: {
				type: Sequelize.DataTypes.STRING(25),
				allowNull: false,
				unique: true
			},
			email: {
				type: Sequelize.DataTypes.STRING(50),
				allowNull: false,
				unique: true
			},
			password: {
				type: Sequelize.DataTypes.STRING(255),
				allowNull: true,
			},
			remember_token: Sequelize.DataTypes.TEXT,
			createdAt: {
				type: Sequelize.DataTypes.DATE,
				allowNull: false,
				defaultValue: Sequelize.fn('NOW')
			},
			updatedAt: {
				type: Sequelize.DataTypes.DATE,
				allowNull: false,
				defaultValue: Sequelize.fn('NOW')
			}
		}, { timestamps: true })
	},

	async down(queryInterface, Sequelize) {
		/**
		 * Add reverting commands here.
		 *
		 * Example:
		 * await queryInterface.dropTable('users');
		 */
		await queryInterface.dropTable('users')
	}
};
