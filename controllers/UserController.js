import Validator from "fastest-validator"
import bcrypt from "bcrypt"
import { Op } from "sequelize"
import Role from "../models/Role.js"
import User from "../models/User.js"
import Post from "../models/Post.js"

User.belongsTo(Role)
User.hasMany(Post)

export const all = async (req, res) => {
	try {
		const users = await User.findAll({
			include: [{ model: Role }]
		})
		res.json(users)
	} catch (error) {
		console.log(error)
		res.status(404).json({ error, message: "Something's wrong" })
	}
}

const schema = {
	name: { type: 'string', min: 3 },
	roleId: { type: "string", label: "User Role" },
	username: { type: 'string', min: 5 },
	email: { type: "email", label: "email address" },
	password: { type: 'string', min: 6 },
}

const schemaUpdate = {
	name: { type: 'string', min: 3 },
	roleId: { type: "string", label: "User Role" },
	username: { type: 'string', min: 5 },
	email: { type: "email", label: "email address" },
}

export const store = async (req, res) => {
	try {
		const { name, username, email, password } = req.body
		const roleId = +req.body.roleId
		const checkUsername = await User.findOne({ where: { username } })
		const checkEmail = await User.findOne({ where: { email } })
		const encrypted = await bcrypt.hash(password, 10)
		const v = new Validator({
			messages: {
				string: "'{field}' required"
			}
		})
		const check = v.compile(schema)
		const validate = check(req.body)
		if (checkUsername != null) {
			res.send({
				status: false,
				message: 'Username already used !!'
			})
		} else if (checkEmail != null) {
			res.send({
				status: false,
				message: 'E-mail already used !!'
			})
		} else {
			if (validate === true) {
				const user = await User.create({ name, username, email, password: encrypted, roleId })
				res.json({
					status: true,
					type: 'success',
					message: 'User Added Successfully',
					user: user.toJSON()
				})
			} else {
				res.json(validate)
			}
		}
	} catch (error) {
		console.log(error)
		res.status(404).json({ error, message: "Something's wrong" })
	}
}

export const show = async (req, res) => {
	try {
		const user = await User.findOne({
			where: {
				id: req.params.id
			},
			include: [{ model: Role }]
		})
		res.json(user)
	} catch (error) {
		console.log(error)
		res.status(404).json({ error, message: "Something's wrong" })
	}
}

export const showByUsername = async (req, res) => {
	try {
		const user = await User.findOne({
			attributes: ['name', 'username', 'id'],
			where: {
				username: req.params.username
			},
			include: [
				{
					model: Post
				}
			],
			order: [
				[Post, 'createdAt', 'DESC']
			]
		})
		res.json(user)
	} catch (error) {
		console.log(error)
		res.status(404).json({ error, message: "Something's wrong" })
	}
}

export const update = async (req, res) => {
	try {
		const { name, username, email, password } = req.body
		const roleId = +req.body.roleId
		let encrypted = null
		let toUpdate = { name, username, email, roleId }
		if (password) {
			encrypted = await bcrypt.hash(password, 10)
			toUpdate = { name, username, email, password: encrypted, roleId }
		}
		const v = new Validator({
			messages: {
				string: "'{field}' required"
			}
		})
		const check = v.compile(schemaUpdate)
		const validate = check(req.body)
		if (validate === true) {
			const user = await User.update(toUpdate, {
				where: {
					id: req.params.id
				}
			})
			if (user) {
				res.json({
					status: true,
					type: 'success',
					message: 'User Updated Successfully'
				})
			} else {
				res.json({
					status: false,
					type: 'error',
					message: 'Failed When Updating User'
				})
			}
		} else {
			res.json(validate)
		}
	} catch (error) {
		console.log(error)
		res.status(404).json({ error, message: "Something's wrong" })
	}
}

export const destroy = async (req, res) => {
	try {
		const user = await User.destroy({
			where: {
				id: req.params.id
			}
		})
		if (user) {
			res.json({
				status: true,
				type: 'success',
				message: 'User Deleted Successfully'
			})
		} else {
			res.json({
				status: false,
				type: 'error',
				message: 'Failed When Deleting User'
			})
		}
	} catch (error) {
		console.log(error)
		res.status(404).json({ error, message: "Something's wrong" })
	}
}

export const search = async (req, res) => {
	try {
		const limit = parseInt(req.query.limit) || 20
		const search = req.query.search || ""
		const result = await User.findAll({
			attributes: ['username', 'name'],
			where: {
				[Op.or]: [
					{ username: { [Op.like]: `%${search}%` } },
					{ name: { [Op.like]: `%${search}%` } }
				],
				id: {
					[Op.ne]: req.body.except
				}
			},
			limit: limit,
			order: [
				['username', 'ASC'],
				['name', 'ASC']
			]
		})
		res.json({ result })
	} catch (error) {
		console.log(error)
		res.status(404).json({ error, message: "Something's wrong" })
	}
}