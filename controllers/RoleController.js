import Validator from "fastest-validator"
import Role from "../models/Role.js"
import User from "../models/User.js"

Role.hasMany(User)

export const all = async (req, res) => {
	try {
		const roles = await Role.findAll()
		res.json(roles)
	} catch (error) {
		res.status(404).json({ error, message: "Something's wrong" })
	}
}
const schema = {
	name: { type: 'string', min: 4 }
}

export const store = async (req, res) => {
	try {
		const v = new Validator()
		const check = v.compile(schema)
		const validate = check(req.body)
		if (validate === true) {
			const role = await Role.create(req.body)
			res.json({
				status: true,
				type: 'success',
				message: 'Role Added Successfully',
				role: role.toJSON()
			})
		} else {
			res.json(validate)
		}
	} catch (error) {
		console.log(error)
	}
}

export const show = async (req, res) => {
	try {
		const role = await Role.findOne({
			where: {
				id: req.params.id
			}
		})
		res.json(role)
	} catch (error) {
		console.log(error)
	}
}

export const update = async (req, res) => {
	try {
		const v = new Validator()
		const check = v.compile(schema)
		const validate = check(req.body)
		if (validate === true) {
			const role = await Role.update(req.body, {
				where: {
					id: req.params.id
				}
			})
			if (role) {
				res.json({
					status: true,
					type: 'success',
					message: 'Role Updated Successfully'
				})
			} else {
				res.json({
					status: false,
					type: 'error',
					message: 'Failed When Updating Role'
				})
			}
		} else {
			res.json(validate)
		}
	} catch (error) {
		console.log(error)
	}
}

export const destroy = async (req, res) => {
	try {
		const role = await Role.destroy({
			where: {
				id: req.params.id
			}
		})
		if (role) {
			res.json({
				status: true,
				type: 'success',
				message: 'Role Deleted Successfully'
			})
		} else {
			res.json({
				status: false,
				type: 'error',
				message: 'Failed When Deleting Role'
			})
		}
	} catch (error) {
		console.log(error)
	}
}