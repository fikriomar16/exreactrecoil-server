import Follow from "../models/Follow.js"

export const checkFollowingUser = async (req, res) => {
	try {
		const check = await Follow.findOne({
			where: {
				userId: +req.params.userId,
				following_userId: +req.params.following_userId,
			}
		})
		if (check) {
			res.json({
				status: false,
				type: 'warning',
				message: 'User Already Followed'
			})
		} else {
			res.json({
				status: true,
				type: 'success',
				message: 'User Not Followed'
			})
		}
	} catch (error) {
		res.status(404).json({ error, message: "Something's wrong" })
	}
}

export const doFollowUser = async (req, res) => {
	try {
		const { userId, following_userId } = req.params
		const follow_user = await Follow.create({ userId, following_userId })
		if (follow_user) {
			res.json({
				status: true,
				type: 'success',
				message: 'User Followed'
			})
		} else {
			res.json({
				status: false,
				type: 'error',
				message: ''
			})
		}
	} catch (error) {
		res.status(404).json({ error, message: "Something's wrong" })
	}
}

export const doUnfollowUser = async (req, res) => {
	try {
		const { userId, following_userId } = req.params
		const unfollow_user = await Follow.destroy({
			where: {
				userId: userId,
				following_userId: following_userId,
			}
		})
		if (unfollow_user) {
			res.json({
				status: true,
				type: 'success',
				message: 'User Unfollowed'
			})
		} else {
			res.json({
				status: false,
				type: 'error',
				message: ''
			})
		}
	} catch (error) {
		res.status(404).json({ error, message: "Something's wrong" })
	}
}

export const countFollower = async (req, res) => {
	try {
		const count_follower = await Follow.count({
			where: {
				following_userId: +req.params.userId
			}
		})
		res.json({ count: count_follower })
	} catch (error) {
		res.status(404).json({ error, message: "Something's wrong" })
	}
}

export const countFollowing = async (req, res) => {
	try {
		const count_following = await Follow.count({
			where: {
				userId: +req.params.userId
			}
		})
		res.json({ count: count_following })
	} catch (error) {
		res.status(404).json({ error, message: "Something's wrong" })
	}
}