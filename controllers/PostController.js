import Validator from "fastest-validator"
import User from "../models/User.js"
import Post from "../models/Post.js"
import Like from "../models/Like.js"
import Follow from "../models/Follow.js"
import Slugger from "../helpers/Slugger.js"
import { unlink } from "node:fs"
import { Op } from "sequelize"

Post.belongsTo(User)
User.hasMany(Post)

Post.hasMany(Like)

export const all = async (req, res) => {
	try {
		const posts = await Post.findAll({
			include: User
		})
		res.json(posts)
	} catch (error) {
		res.status(404).json({ error, message: "Something's wrong" })
	}
}

export const allBy = async (req, res) => {
	try {
		const posts = await Post.findAll({
			where: {
				userId: +req.params.userId
			},
			order: [
				['createdAt', 'DESC']
			],
			include: User
		})
		res.json(posts)
	} catch (error) {
		res.status(404).json({ error, message: "Something's wrong" })
	}
}

export const store = async (req, res) => {
	try {
		const schema = {
			title: { type: 'string', min: 5 },
			userId: { type: 'number', label: "User" },
		}
		const { filename, mimetype, path } = req.file
		const { title, caption } = req.body
		const userId = +req.body.userId
		const v = new Validator({
			messages: {
				number: "'{field}' required"
			}
		})
		const check = v.compile(schema)
		const validate = check({ title, userId })
		const slug = Slugger(title) + "-" + Date.now()
		if (validate === true) {
			if (req.file) {
				const toInsert = { title, slug, caption, photo: filename, userId }
				const post = await Post.create(toInsert)
				if (post) {
					await res.json({
						status: true,
						type: 'success',
						message: 'Post Created',
						post: post.toJSON()
					})
				} else {
					await res.json({
						status: false,
						type: 'error',
						message: 'Failed when making post !!'
					})
				}
			} else {
				await res.status(401).json({ error: 'Please provide an image' })
			}
		} else {
			unlink(path, (err) => {
				if (err) console.log(err)
			})
			await res.json(validate)
		}
	} catch (error) {
		res.status(404).json({ error, message: "Something's wrong" })
	}
}

export const destroy = async (req, res) => {
	try {
		const post = await Post.findOne({
			where: {
				id: req.params.id
			}
		})
		if (post) {
			unlink(`images/${post.photo}`, (err) => {
				if (err) console.log(err)
				console.log('deleted')
			})
			const deletePost = await Post.destroy({
				where: {
					id: req.params.id
				}
			})
			if (deletePost) {
				res.json({
					status: true,
					type: 'success',
					message: 'Post Deleted Successfully'
				})
			} else {
				res.json({
					status: false,
					type: 'error',
					message: 'Failed When Deleting Post'
				})
			}
		} else {
			res.json({
				status: false,
				type: 'error',
				mesage: 'Post Not Foud !!'
			})
		}
	} catch (error) {
		res.status(404).json({ error, message: "Something's wrong" })
	}
}

export const allFor = async (req, res) => {
	try {
		Follow.belongsTo(User, { foreignKey: 'following_userId' })
		User.hasMany(Follow, { foreignKey: 'following_userId' })
		const posts = await Post.findAll({
			include: [
				{
					model: User,
					attributes: ['id', 'name', 'username'],
					include: [
						{
							model: Follow,
							attributes: []
						}
					]
				}
			],
			where: {
				[Op.or]: {
					["$user.follows.userId$"]: req.params.userId,
					userId: req.params.userId,
				}
			},
			order: [
				['createdAt', 'DESC']
			]
		})
		res.json(posts)
	} catch (error) {
		res.status(404).json({ error, message: "Something's wrong" })
	}
}

export const countPosts = async (req, res) => {
	try {
		const count_posts = await Post.count({
			where: {
				userId: +req.params.userId
			}
		})
		res.json({ count: count_posts })
	} catch (error) {
		res.status(404).json({ error, message: "Something's wrong" })
	}
}