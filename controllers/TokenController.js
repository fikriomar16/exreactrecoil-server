import jwt from "jsonwebtoken"
import Role from "../models/Role.js"
import User from "../models/User.js"

export const refreshToken = async (req, res) => {
	try {
		const refreshToken = req.cookies.refreshToken
		if (!refreshToken) return res.sendStatus(401)
		const getUser = await User.findOne({
			where: {
				remember_token: refreshToken
			},
			include: Role
		})
		if (!getUser) return res.sendStatus(403)
		const { id, name, username, email, roleId } = getUser
		const role = getUser.role.name.toLowerCase()
		await jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, async (err, decoded) => {
			if (err) return res.sendStatus(403)
			const accessToken = await jwt.sign({ id, name, username, email, roleId, role }, process.env.ACCESS_TOKEN_SECRET, {
				expiresIn: '1h'
			});
			res.json({ accessToken })
		})
	} catch (error) {
		console.log(error)
	}
}