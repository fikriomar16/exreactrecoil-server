import bcrypt from "bcrypt"
import jwt from "jsonwebtoken"
import Validator from "fastest-validator"
import Role from "../models/Role.js"
import User from "../models/User.js"

const schemaLogin = {
	username: { type: 'string', min: 5 },
	password: { type: 'string', min: 6 },
}
const schemaRegister = {
	name: { type: 'string', min: 3 },
	roleId: { type: "number", positive: true, integer: true, label: "User Role" },
	username: { type: 'string', min: 5 },
	email: { type: "email", label: "Email Address" },
	password: { type: 'string', min: 6 },
}

export const Login = async (req, res) => {
	try {
		const v = new Validator()
		const check = v.compile(schemaLogin)
		const validate = check(req.body)
		if (validate === true) {
			const getUser = await User.findOne({
				where: {
					username: req.body.username
				},
				include: Role
			})
			if (getUser === null || !getUser) return res.status(400).json({ error: "User Not Found" })
			const match = await bcrypt.compare(req.body.password, getUser.password)
			if (!match) return res.status(400).json({ error: "Wrong Password" })
			const { id, name, username, email, roleId } = getUser
			const role = getUser.role.name.toLowerCase()
			const accessToken = await jwt.sign({ id, name, username, email, roleId, role }, process.env.ACCESS_TOKEN_SECRET, {
				expiresIn: '1h'
			})
			const refreshToken = await jwt.sign({ id, name, username, email, roleId, role }, process.env.REFRESH_TOKEN_SECRET, {
				expiresIn: '1d'
			})
			await User.update({ remember_token: refreshToken }, {
				where: { id }
			})
			res.cookie('refreshToken', refreshToken, {
				httpOnly: true,
				maxAge: 24 * 60 * 60 * 1000
			})
			res.json({ accessToken })
		} else {
			res.json(validate)
		}
	} catch (error) {
		res.status(404).json({ error, message: "Something's wrong" })
	}
}

export const Logout = async (req, res) => {
	const { refreshToken } = req.cookies
	if (!refreshToken) return res.sendStatus(204)
	const getUser = await User.findOne({
		where: {
			remember_token: refreshToken
		}
	})
	if (!getUser) return res.sendStatus(204)
	const { id } = getUser
	await User.update({ remember_token: null }, {
		where: { id }
	})
	res.clearCookie('refreshToken')
	return res.sendStatus(200)
}

export const Register = async (req, res) => {
	try {
		const saltRounds = 10
		const { name, username, email, password } = req.body
		const roleId = +req.body.roleId
		const v = new Validator({
			messages: {
				numberPositive: "'{field}' required"
			}
		})
		const check = v.compile(schemaRegister)
		const validate = check({ name, username, email, password, roleId })
		const encrypted = await bcrypt.hash(password, saltRounds)
		const checkUsername = await User.findOne({ where: { username } })
		const checkEmail = await User.findOne({ where: { email } })
		if (validate === true) {
			if (checkUsername != null) {
				res.send({
					status: false,
					message: 'Username already used !!'
				})
			} else if (checkEmail != null) {
				res.send({
					status: false,
					message: 'E-mail already used !!'
				})
			} else {
				const register = await User.create({ name, username, email, roleId, password: encrypted })
				if (register) {
					res.send({
						status: true,
						message: 'Registration Success !!',
						user: register
					})
				} else {
					res.send({
						status: false,
						message: 'Registration Failed !!'
					})
				}
			}
		} else {
			res.json(validate)
		}
	} catch (error) { res.status(404).json({ error, message: "Something's wrong" }) }
}