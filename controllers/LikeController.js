import Post from "../models/Post.js"
import Like from "../models/Like.js"

Like.belongsTo(Post)

export const checkLikePost = async (req, res) => {
	try {
		const check = await Like.findOne({
			where: {
				userId: +req.params.userId,
				postId: +req.params.postId
			}
		})
		if (check) {
			res.json({
				status: false,
				type: 'warning',
				message: 'Post Already Liked'
			})
		} else {
			res.json({
				status: true,
				type: 'success',
				message: 'Post Not Liked'
			})
		}
	} catch (error) {
		res.status(404).json({ error, message: "Something's wrong" })
	}
}

export const likePost = async (req, res) => {
	try {
		const { userId, postId } = req.params
		const like_post = await Like.create({ userId, postId })
		if (like_post) {
			res.json({
				status: true,
				type: 'success',
				message: 'Post Liked'
			})
		} else {
			res.json({
				status: false,
				type: 'error',
				message: ''
			})
		}
	} catch (error) {
		res.status(404).json({ error, message: "Something's wrong" })
	}
}
export const dislikePost = async (req, res) => {
	try {
		const { userId, postId } = req.params
		const dislike_post = await Like.destroy({
			where: {
				userId: userId,
				postId: postId,
			}
		})
		if (dislike_post) {
			res.json({
				status: true,
				type: 'success',
				message: 'Post DisLiked'
			})
		} else {
			res.json({
				status: false,
				type: 'error',
				message: ''
			})
		}
	} catch (error) {
		res.status(404).json({ error, message: "Something's wrong" })
	}
}

export const countLike = async (req, res) => {
	try {
		const countLikePost = await Like.count({
			where: {
				postId: req.params.postId
			}
		})
		res.json({ count: countLikePost })
	} catch (error) {
		res.status(404).json({ error, message: "Something's wrong" })
	}
}