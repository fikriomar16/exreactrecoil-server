import multer from 'multer'
import { basename as _basename, extname, join } from 'path'
import { fileURLToPath } from 'url'

const __filename = fileURLToPath(import.meta.url)
const basename = _basename(__filename)

const whitelist = [
	'image/png',
	'image/jpeg',
	'image/jpg',
	'image/webp',
	'image/gif'
]

const storage = multer.diskStorage({
	destination: function (req, file, cb) {
		cb(null, join(basename, '../images'))
	},
	filename: function (req, file, cb) {
		cb(null, `${Date.now()}-image${extname(file.originalname)}`);
	}
})
const upload = multer({
	storage: storage,
	limits: {
		fileSize: 4 * 1024 * 1024,
	},
	fileFilter: function (req, file, cb) {
		if (whitelist.includes(file.mimetype)) {
			cb(null, true)
		} else {
			req.errorExt = {
				error: "Not an image file",
				name: "MulterError",
				message: "File is not allowed",
				field: "photo"
			}
			cb(null, false, new Error("File isn't allowed"))
			// cb(null, false)
		}
	}
})

const multerUpload = upload.single('photo')

const multerHelper = (req, res, next) => {
	multerUpload(req, res, function (err) {
		if (err instanceof multer.MulterError) {
			console.log(err)
			return res.status(403).send(err)
		} else if (err) {
			console.log(err)
			return res.status(403).send(err)
		} else if (req.errorExt) {
			console.log(req.errorExt)
			return res.status(403).send(req.errorExt)
		}
		next()
	})
}

export default multerHelper